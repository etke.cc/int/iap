# github.com/VictoriaMetrics/metrics v1.34.0
## explicit; go 1.17
github.com/VictoriaMetrics/metrics
# github.com/archdx/zerolog-sentry v1.8.3
## explicit; go 1.20
github.com/archdx/zerolog-sentry
# github.com/buger/jsonparser v1.1.1
## explicit; go 1.13
github.com/buger/jsonparser
# github.com/getsentry/sentry-go v0.28.1
## explicit; go 1.18
github.com/getsentry/sentry-go
github.com/getsentry/sentry-go/internal/debug
github.com/getsentry/sentry-go/internal/otel/baggage
github.com/getsentry/sentry-go/internal/otel/baggage/internal/baggage
github.com/getsentry/sentry-go/internal/ratelimit
github.com/getsentry/sentry-go/internal/traceparser
# github.com/golang-jwt/jwt v3.2.2+incompatible
## explicit
github.com/golang-jwt/jwt
# github.com/google/uuid v1.6.0
## explicit
github.com/google/uuid
# github.com/hashicorp/golang-lru/v2 v2.0.7
## explicit; go 1.18
github.com/hashicorp/golang-lru/v2/expirable
github.com/hashicorp/golang-lru/v2/internal
# github.com/labstack/echo/v4 v4.12.0
## explicit; go 1.18
github.com/labstack/echo/v4
github.com/labstack/echo/v4/middleware
# github.com/labstack/gommon v0.4.2
## explicit; go 1.18
github.com/labstack/gommon/bytes
github.com/labstack/gommon/color
github.com/labstack/gommon/log
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.20
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/mileusna/useragent v1.3.4
## explicit; go 1.14
github.com/mileusna/useragent
# github.com/rs/zerolog v1.33.0
## explicit; go 1.15
github.com/rs/zerolog
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
# github.com/valyala/bytebufferpool v1.0.0
## explicit
github.com/valyala/bytebufferpool
# github.com/valyala/fastrand v1.1.0
## explicit
github.com/valyala/fastrand
# github.com/valyala/fasttemplate v1.2.2
## explicit; go 1.12
github.com/valyala/fasttemplate
# github.com/valyala/histogram v1.2.0
## explicit; go 1.12
github.com/valyala/histogram
# github.com/ziflex/lecho/v3 v3.7.0
## explicit; go 1.17
github.com/ziflex/lecho/v3
# gitlab.com/etke.cc/go/apm v1.2.5
## explicit; go 1.21.0
gitlab.com/etke.cc/go/apm
# gitlab.com/etke.cc/go/echo-basic-auth v1.1.0
## explicit; go 1.21.0
gitlab.com/etke.cc/go/echo-basic-auth
# gitlab.com/etke.cc/go/env v1.2.0
## explicit; go 1.18
gitlab.com/etke.cc/go/env
gitlab.com/etke.cc/go/env/dotenv
# gitlab.com/etke.cc/go/healthchecks/v2 v2.2.0
## explicit; go 1.18
gitlab.com/etke.cc/go/healthchecks/v2
# golang.org/x/crypto v0.24.0
## explicit; go 1.18
golang.org/x/crypto/acme
golang.org/x/crypto/acme/autocert
# golang.org/x/net v0.26.0
## explicit; go 1.18
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/h2c
golang.org/x/net/http2/hpack
golang.org/x/net/idna
# golang.org/x/sys v0.21.0
## explicit; go 1.18
golang.org/x/sys/execabs
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.16.0
## explicit; go 1.18
golang.org/x/text/cases
golang.org/x/text/internal
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# golang.org/x/time v0.5.0
## explicit; go 1.18
golang.org/x/time/rate
